//
// Created by Daniel on 19. 12. 2019.
//

#ifndef PROJEKT2_0_VYROBA_H
#define PROJEKT2_0_VYROBA_H


#include <iostream>
#include "Vyrobky.h"
using namespace std;

class Vyroba {
private:
    string m_nazev;
    float m_pracovniDoba;
    float m_cenaNakupu;
    float m_cenaProdeje;
    float m_aktualniPracDoba;

public:
    void vyrobVyrobek(Vyrobky* vyrobky);
};


#endif //PROJEKT2_0_VYROBA_H
