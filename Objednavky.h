//
// Created by Daniel on 19. 12. 2019.
//

#ifndef PROJEKT2_0_OBJEDNAVKY_H
#define PROJEKT2_0_OBJEDNAVKY_H

#include <iostream>
#include "Vyroba.h"
#include "Vyrobky.h"

class Objednavky {
private:
    int m_pocetObjednavek;
    int m_nakoupeneStroje;

public:
    Objednavky();
    void kupStroj(std::string nazev, float pracovniDoba, float cenaNakupu);
    void prodejSroj(Vyroba* stroj);
    void nakupVyrobku(Vyrobky* vyrobek, float mnozstvi);

    ~Objednavky();


};


#endif //PROJEKT2_0_OBJEDNAVKY_H
