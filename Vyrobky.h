//
// Created by Daniel on 19. 12. 2019.
//

#ifndef PROJEKT2_0_VYROBKY_H
#define PROJEKT2_0_VYROBKY_H


#include <iostream>
#include <vector>
#include "Material.h"
using namespace std;

class Vyrobky {
private:
    string m_nazev;
    int m_mnozstvi;
    // vector<Material*> m_spotreba;
    float m_skladCena;
    float m_dobaVyroby;
    float m_prodejniCena;
    float m_mnozstvniSpotreba;

public:
    Vyrobky(string nazev, /* yase ten vector */float skladCena, float dobaVyroby, float matroSpotreba, float prodejniCena);

    string getNazev();
    void setNazev(string newName);
    float getMnozstvi();
    void setMnozstvi(float mnozstvi);
    //float getVectorSpotreba;
    //  void setVectorSpotreba(string Material);
    float getSkladCena();
    void setSkladCena(float NovaCena);
    float getDobaVyroby();
    void setDobaVyroby(float novaDoba);
    // float getVectorMnozstvi();
    // void setVectorMnozstvi(Vyrobky vyrobek, float noveMnozstvi);
    float getProdejniCena();
    void setProdejniCena(float novaCena);
};


#endif //PROJEKT2_0_VYROBKY_H
