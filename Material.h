//
// Created by Daniel on 21. 12. 2019.
//

#ifndef PROJEKT_XKONICE1_MATERIAL_H
#define PROJEKT_XKONICE1_MATERIAL_H


#include <iostream>
using namespace std;

class Material {
private:
    string m_nazev;
    float m_cena;
    float m_cenaSklad;

public:
    Material(float cenaKilo, float vkladCena);
    void nakupMaterial(string nazev, float cenaKilo);
    // void deleteMaterial(Material);
    void setNazev(string newName);
    string getNazev();
    void setCenaKilo(float newCena);
    float getCenaKilo();
    void setSkladCenaKilo(float cena);
    float getSkladCenaKilo();


};


#endif //PROJEKT_XKONICE1_MATERIAL_H
