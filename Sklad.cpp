//
// Created by Daniel on 21. 12. 2019.
//

#include "Sklad.h"

Sklad::Sklad(){
    m_penize = 100000;
    m_poplatek = 0;
    m_matMnozstvi.push_back(0);
    m_matMnozstvi.push_back(0);
    m_matMnozstvi.push_back(0);
}

float Sklad::spoctiPoplatek(float hodnota, int id) {
    return (m_matMnozstvi[id] * hodnota);
}

Sklad::~Sklad() {

}
void Sklad::pridatMaterial(Material* mat){
    m_materialy.push_back(mat);
}
void Sklad::odecistPenize(float penize){
    m_penize -= penize;
}
float Sklad::getPenize(){
    return m_penize;
}
void Sklad::pridatKila(int id, float pocet){
    m_matMnozstvi[id]+=pocet;
}
void Sklad::vypisSurovin(){
    cout << "Drevo: " << m_matMnozstvi[0] << ", Zelezo: " << m_matMnozstvi[1] << ", Hlina: " << m_matMnozstvi[2] << endl;
}