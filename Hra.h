//
// Created by Daniel on 21. 12. 2019.
//

#ifndef PROJEKT_XKONICE1_HRA_H
#define PROJEKT_XKONICE1_HRA_H

#include <iostream>
using namespace std;

#include "Material.h"
#include "Sklad.h"

class Hra {
private:
    int m_pocetKol;

public:
    Hra();
    void obnovaStrojCasu();
    void resetObjednavek();
    void kontrolaPenez();
    void hraMenu();

    ~Hra();
};;


#endif //PROJEKT_XKONICE1_HRA_H
