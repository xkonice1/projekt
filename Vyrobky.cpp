//
// Created by Daniel on 19. 12. 2019.
//

#include "Vyrobky.h"

Vyrobky::Vyrobky(string nazev, /* yase ten vector */float skladCena, float dobaVyroby, float matroSpotreba, float prodejniCena){
    m_nazev=nazev;
    m_skladCena=skladCena;
    m_dobaVyroby=dobaVyroby;
    m_mnozstvniSpotreba=matroSpotreba;
    m_prodejniCena=prodejniCena;
    m_mnozstvi = 0;
}

string Vyrobky::getNazev(){
    return m_nazev;
}

void Vyrobky::setNazev(string newName){
    m_nazev = newName;
}

float Vyrobky::getMnozstvi(){
    return m_mnozstvi;
}

void Vyrobky::setMnozstvi(float mnozstvi){
    m_mnozstvi = mnozstvi;
}

/*void Vyrobky::getVectorSpotreba(){
    return Vyrobky;
}

void Vyrobky::setVectorSpotreba(string Material){

}*/

float Vyrobky::getSkladCena(){
    return m_skladCena;
}
void Vyrobky::setSkladCena(float NovaCena){
    m_skladCena = NovaCena;
}

float Vyrobky::getDobaVyroby(){
    return m_dobaVyroby;
}
void Vyrobky::setDobaVyroby(float novaDoba){
    m_dobaVyroby = novaDoba;
}
/*
float Vyrobky::getVectorMnozstvi(){
    return vectorMnozstvi;
}
Vyrobky::setVectorMnozstvi(Vyrobky vyrobek, float noveMnozstvi){

}*/

float Vyrobky::getProdejniCena(){
    return m_prodejniCena;
}
void Vyrobky::setProdejniCena(float novaCena){
    m_prodejniCena = novaCena;
}