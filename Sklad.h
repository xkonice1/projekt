//
// Created by Daniel on 21. 12. 2019.
//

#ifndef PROJEKT_XKONICE1_SKLAD_H
#define PROJEKT_XKONICE1_SKLAD_H

using namespace std;
#include "Material.h"
#include <vector>

class Sklad {


private:
    float m_penize;
    float m_poplatek;
    vector<Material*> m_materialy;
    vector<float> m_matMnozstvi;
    //vector<Vyrobky*>m_vyrobky;
    //vector<Vyroba*>m_stroje;

public:
    Sklad();
    float spoctiPoplatek(float hodnota,int id);
    void pridatMaterial(Material* mat);
    void odecistPenize(float penize);
    float getPenize();
    void pridatKila(int id, float pocet);
    void vypisSurovin();
    ~Sklad();
};


#endif //PROJEKT_XKONICE1_SKLAD_H
