//
// Created by Daniel on 21. 12. 2019.
//

#include "Hra.h"


Hra::Hra() {
    m_pocetKol = 0;
}

void Hra::obnovaStrojCasu() {

}

void Hra::resetObjednavek() {

}

void Hra::kontrolaPenez(){

}

Hra::~Hra() {

}

void Hra::hraMenu(){
    Sklad* sklad = new Sklad();
    Material* drevo = new Material(100,50);
    Material* zelezo = new Material(80,40);
    Material* hlina = new Material(60,30);
    sklad->pridatMaterial(drevo);
    sklad->pridatMaterial(zelezo);
    sklad->pridatMaterial(hlina);
    int x;
    cout << "1 pro ukonceni kola, 2 pro vypis informaci, 3 pro nakup surovin, 4 pro ukonceni: " << endl;
    cin >> x;
    while(x != 4){
        if(x == 1){
            obnovaStrojCasu();
            m_pocetKol++;
            sklad->odecistPenize(sklad->spoctiPoplatek(drevo->getSkladCenaKilo(),0));
            sklad->odecistPenize(sklad->spoctiPoplatek(zelezo->getSkladCenaKilo(),1));
            sklad->odecistPenize(sklad->spoctiPoplatek(hlina->getSkladCenaKilo(),2));
        }
        else if(x==2){
            cout << "Penize: " << sklad->getPenize() << endl;
            sklad->vypisSurovin();
        }
        else if(x == 3) {
            cout << "Zadejte 1 pro drevo, 2 pro zelezo, 3 pro hlinu a 4 pro zpet" << endl;
            int y;
            cin >> y;
            while (y != 4) {
                if (y == 1) {
                    float z;
                    cout << "Zadejte pocet kilo: " << endl;
                    cin >> z;
                    if (sklad->getPenize() >= (drevo->getCenaKilo() * z)) {
                        sklad->pridatKila(0, z);
                        sklad->odecistPenize(z * (drevo->getCenaKilo()));
                    } else {
                        cout << "Nedostatek penez" << endl;
                    }
                } else if (y == 2) {
                    float z;
                    cout << "Zadejte pocet kilo: " << endl;
                    cin >> z;
                    if (sklad->getPenize() >= (zelezo->getCenaKilo() * z)) {
                        sklad->pridatKila(1, z);
                        sklad->odecistPenize(z * (zelezo->getCenaKilo()));
                    } else {
                        cout << "Nedostatek penez" << endl;
                    }
                } else if (y == 3) {
                    float z;
                    cout << "Zadejte pocet kilo: " << endl;
                    cin >> z;
                    if (sklad->getPenize() >= (hlina->getCenaKilo() * z)) {
                        sklad->pridatKila(2, z);
                        sklad->odecistPenize(z * (hlina->getCenaKilo()));
                    } else {
                        cout << "Nedostatek penez" << endl;
                    }
                }
                cout << "Zadejte 1 pro drevo, 2 pro zelezo, 3 pro hlinu a 4 pro zpet" << endl;
                cin >> y;
            }
        }
        cout << "1 pro ukonceni kola, 2 pro vypis informaci, 3 pro nakup surovin, 4 pro ukonceni: " << endl;
        cin >> x;
    }
}